package com.example.mbiletask.utils.di

import android.app.Application
import androidx.room.Room
import com.example.mbiletask.data.local.LocalDataSource
import com.example.mbiletask.data.local.LocalDataSourceImp
import com.example.mbiletask.data.local.GistsDB
import com.example.mbiletask.data.local.mapper.GistLocalMapper
import com.example.mbiletask.data.local.model.GistLocal
import com.example.mbiletask.data.remote.model.Gist
import com.example.mbiletask.utils.Mapper
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [LocalPersistenceModule.Binders::class])
class LocalPersistenceModule {

    @Module
    interface Binders {

        @Binds
        fun bindsLocalDataSource(
            localDataSourceImpl: LocalDataSourceImp
        ): LocalDataSource

        @Binds
        fun bindRecordsLocalMapper(
            CGistLocalMapper: GistLocalMapper
        ): Mapper<Gist, GistLocal>

    }

    @Provides
    @Singleton
    fun providesDatabase(
        application: Application
    ) = Room.inMemoryDatabaseBuilder(application, GistsDB::class.java)
        .allowMainThreadQueries()
        .build()

    @Provides
    @Singleton
    fun providesRecordsDAO(
        gistsDB: GistsDB
    ) = gistsDB.getRecordsDAO()


}
