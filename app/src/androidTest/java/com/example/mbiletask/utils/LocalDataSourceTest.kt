package com.example.mbiletask.utils

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.platform.app.InstrumentationRegistry
import com.example.mbiletask.data.local.GistsDAO
import com.example.mbiletask.data.local.GistsDB
import com.example.mbiletask.utils.utils.TestData
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4ClassRunner::class)
class LocalDataSourceTest {

    private lateinit var gistsDB: GistsDB
    private lateinit var gistsDAO: GistsDAO

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        gistsDB = Room.inMemoryDatabaseBuilder(context, GistsDB::class.java)
            .allowMainThreadQueries()
            .build()

        gistsDAO = gistsDB.getRecordsDAO()
    }

    @After
    fun tearDown() {
        gistsDB.close()
    }

    @Test
    fun test_saveAndRetrieveRecords() {
        val records = TestData.generateLocalRecords()
        val recordsCount = records.size

        gistsDAO.saveGists(records)

        gistsDAO.getGists()
            .test()
            .assertValue {
                records.containsAll(it)
                        && it.size == recordsCount
            }.assertNotComplete() // As Room Observables are kept alive
    }


}