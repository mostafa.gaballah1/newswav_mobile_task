package com.example.mbiletask.utils.utils

import com.example.mbiletask.data.local.model.GistLocal

class TestData {

    companion object {

        fun generateLocalRecords(): List<GistLocal> {
            return listOf(
                GistLocal("1","2009-Q1","1.22",1),
                GistLocal("2","2009-Q2","1.33",0),
                GistLocal("3","2009-Q3","1.44",0),
                GistLocal("4","2009-Q4","1.55",0)
            )
        }

    }
}