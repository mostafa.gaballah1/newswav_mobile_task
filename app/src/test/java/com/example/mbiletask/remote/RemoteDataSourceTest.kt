package com.example.mbiletask.remote

import com.example.mbiletask.data.remote.ApiService
import com.example.mbiletask.data.remote.RemoteDataSource
import com.example.mbiletask.data.remote.RemoteDataSourceImp
import com.example.mbiletask.utils.TestDataGenerator
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.times
import org.mockito.MockitoAnnotations

@RunWith(JUnit4::class)
class RemoteDataSourceTest {

    @Mock
    private lateinit var apiService: ApiService
    private lateinit var remoteDataSource: RemoteDataSource

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        remoteDataSource = RemoteDataSourceImp(
            apiService
        )
    }

    @Test
    fun test_getGists_success() {
        val gistsResponse = TestDataGenerator.generateRemoteGists()

        Mockito.`when`(apiService.getGists())
            .thenReturn(Observable.just(gistsResponse))

        remoteDataSource.getGists()
            .test()
            .assertSubscribed()
            .assertValue {
                it == gistsResponse && it[0] == gistsResponse[0]
            }.assertComplete()

        Mockito.verify(apiService, times(1))
            .getGists()
    }

    @Test
    fun test_getGists_error() {
        val errorMsg = "ERROR"

        Mockito.`when`(apiService.getGists())
            .thenReturn(Observable.error(Throwable(errorMsg)))

        remoteDataSource.getGists()
            .test()
            .assertSubscribed()
            .assertError {
                it.message == errorMsg
            }.assertNotComplete()
    }

}