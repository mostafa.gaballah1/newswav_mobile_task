package com.example.mbiletask.local

import com.example.mbiletask.data.local.LocalDataSource
import com.example.mbiletask.data.local.LocalDataSourceImp
import com.example.mbiletask.data.local.GistsDAO
import com.example.mbiletask.data.local.mapper.GistLocalMapper
import com.example.mbiletask.utils.TestDataGenerator
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

@RunWith(JUnit4::class)
class LocalDataSourceTest {

    private val gistLocalMapper = GistLocalMapper()

    @Mock
    private lateinit var gistsDAO: GistsDAO
    private lateinit var localDataSource: LocalDataSource

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        localDataSource = LocalDataSourceImp(gistsDAO, gistLocalMapper)
    }

    @Test
    fun test_getGists_success() {
        val localGists = TestDataGenerator.generateLocalGists()

        Mockito.`when`(gistsDAO.getGists())
            .thenReturn(Observable.just(localGists))

        localDataSource.getGists()
            .test()
            .assertSubscribed()
            .assertValue { gists ->
                localGists.containsAll(
                    gists.map {
                        gistLocalMapper.to(it)
                    }
                )
            }.assertComplete()
    }

    @Test
    fun test_getGists_error() {
        val errorMsg = "ERROR"

        Mockito.`when`(gistsDAO.getGists())
            .thenReturn(Observable.error(Throwable(errorMsg)))

        localDataSource.getGists()
            .test()
            .assertSubscribed()
            .assertError {
                it.message == errorMsg
            }.assertNotComplete()
    }

}