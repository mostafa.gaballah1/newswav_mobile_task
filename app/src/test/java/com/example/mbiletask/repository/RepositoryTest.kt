package com.example.mbiletask.repository

import com.example.mbiletask.data.local.LocalDataSource
import com.example.mbiletask.data.remote.RemoteDataSource
import com.example.mbiletask.data.repository.GistsRepository
import com.example.mbiletask.data.repository.GistsRepositoryImp
import com.example.mbiletask.utils.TestDataGenerator
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

@RunWith(JUnit4::class)
class RepositoryTest {

    @Mock
    private lateinit var remoteDataSource: RemoteDataSource

    @Mock
    private lateinit var localDataSource: LocalDataSource
    private lateinit var repository: GistsRepository


    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        repository = GistsRepositoryImp(
            remoteDataSource,
            localDataSource
        )
    }

    @Test
    fun test_getGists_local_remote_interactions() {
        val gists = TestDataGenerator.generateRemoteGists()

        Mockito.`when`(remoteDataSource.getGists())
            .thenReturn(Observable.just(gists))

        Mockito.`when`(localDataSource.getGists())
            .thenReturn(Observable.just(gists))

        val testSubscriber = repository.getGists().test()

        testSubscriber.assertSubscribed()
            .assertValues(
                gists,
                gists
            )
            .assertComplete()

        Mockito.verify(localDataSource, Mockito.times(1))
            .saveGists(gists)

        Mockito.verify(remoteDataSource, Mockito.times(1))
            .getGists()
    }

    @Test
    fun test_getGists_remote_error() {
        val gists = TestDataGenerator.generateRemoteGists()

        Mockito.`when`(remoteDataSource.getGists())
            .thenReturn(Observable.error(Throwable()))
        Mockito.`when`(localDataSource.getGists())
            .thenReturn(Observable.just(gists))

        val testSubscriber = repository.getGists().test()

        testSubscriber.assertSubscribed()
            .assertValue { res ->
                res.containsAll(gists)
            }
            .assertComplete()

        Mockito.verify(localDataSource, Mockito.times(1))
            .getGists()
    }

}