package com.example.mbiletask.utils

import com.example.mbiletask.data.local.model.GistLocal
import com.example.mbiletask.data.remote.model.Gist
import com.example.mbiletask.data.repository.GistsRepository
import com.example.mbiletask.ui.GistsPresentation
import com.example.mbiletask.usecases.GistsUseCase
import io.reactivex.schedulers.Schedulers

class TestDataGenerator {

    companion object {

        fun generateRemoteGists(): List<Gist> {
            return listOf(
                        Gist("1","2009-Q1"),
                        Gist("2","2009-Q2"),
                        Gist("3","2009-Q3"),
                        Gist("4","2009-Q4")
                    )
        }

        fun generateLocalGists(): List<GistLocal> {
            return listOf(
                GistLocal("1","2009-Q1"),
                GistLocal("2","2009-Q2"),
                GistLocal("3","2009-Q3"),
                GistLocal("4","2009-Q4")
            )
        }

        fun getGistsUseCase(repository: GistsRepository): GistsUseCase {
            return GistsUseCase(
                repository,
                Schedulers.trampoline(),
                Schedulers.trampoline()
            )
        }



        fun generateGistsData(): GistsPresentation {
            return GistsPresentation(
                generateRemoteGists()
            )
        }


    }
}