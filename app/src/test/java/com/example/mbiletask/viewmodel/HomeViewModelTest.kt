package com.example.mbiletask.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.mbiletask.data.Status
import com.example.mbiletask.data.repository.GistsRepository
import com.example.mbiletask.ui.HomeViewModel
import com.example.mbiletask.utils.RxjavaTestUtils
import com.example.mbiletask.utils.TestDataGenerator
import io.reactivex.Observable
import org.junit.*
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

@RunWith(JUnit4::class)
class HomeViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    lateinit var repository: GistsRepository
    @get:Rule
    var rxSchedulersOverrideRule = RxjavaTestUtils()
    private lateinit var homeViewModel: HomeViewModel

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)

        homeViewModel = HomeViewModel(
            TestDataGenerator.getGistsUseCase(repository)
        )
    }

    @Test
    fun test_getGistsData_success() {
        Mockito.`when`(repository.getGists()).thenReturn(Observable.just(TestDataGenerator.generateRemoteGists()))
        homeViewModel.getGistsData()
        val gistsSource = homeViewModel.gistsLiveData

        gistsSource.observeForever {}
        Assert.assertTrue(
            gistsSource.value?.status == Status.SUCCESS
                    && gistsSource.value?.data == TestDataGenerator.generateGistsData()
        )
    }

    @Test
    fun test_getGistsData_error() {
        val errorMsg = "fetch error"

        Mockito.`when`(repository.getGists()).thenReturn(Observable.error(Throwable(errorMsg)))
        homeViewModel.getGistsData()

        val gistsSource = homeViewModel.gistsLiveData
        gistsSource.observeForever {}

        Assert.assertTrue(
            gistsSource.value?.status == Status.ERROR
                    && gistsSource.value?.message == errorMsg
        )
    }

}