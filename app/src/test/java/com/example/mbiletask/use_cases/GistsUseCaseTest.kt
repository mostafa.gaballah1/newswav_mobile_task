package com.example.mbiletask.use_cases

import com.example.mbiletask.data.repository.GistsRepository
import com.example.mbiletask.usecases.GistsUseCase
import com.example.mbiletask.utils.TestDataGenerator
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

@RunWith(JUnit4::class)
class GistsUseCaseTest {

    @Mock
    private lateinit var repository: GistsRepository
    private lateinit var gistsUseCase: GistsUseCase

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        gistsUseCase = GistsUseCase(
            repository,
            Schedulers.trampoline(),
            Schedulers.trampoline()
        )
    }

    @Test
    fun test_getGists_success() {
        val gists = TestDataGenerator.generateRemoteGists()

        Mockito.`when`(repository.getGists())
            .thenReturn(Observable.just(gists))

        val testObserver = gistsUseCase.buildUseCase().test()

        testObserver
            .assertSubscribed()
            .assertValue { it.containsAll(gists) }
    }

    @Test
    fun test_getGists_error() {
        val errorMsg = "ERROR"

        Mockito.`when`(repository.getGists())
            .thenReturn(Observable.error(Throwable(errorMsg)))

        val testObserver = gistsUseCase.buildUseCase().test()

        testObserver
            .assertSubscribed()
            .assertError { it.message?.equals(errorMsg, false) ?: false }
            .assertNotComplete()
    }
}