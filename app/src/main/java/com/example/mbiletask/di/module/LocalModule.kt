package com.example.mbiletask.di.module

import android.app.Application
import com.example.mbiletask.data.local.LocalDataSource
import com.example.mbiletask.data.local.LocalDataSourceImp
import com.example.mbiletask.data.local.GistsDB
import com.example.mbiletask.data.local.mapper.GistLocalMapper
import com.example.mbiletask.data.local.model.GistLocal
import com.example.mbiletask.data.remote.model.Gist
import com.example.mbiletask.utils.Mapper
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [LocalModule.Binders::class])
class LocalModule {

    @Module
    interface Binders {

        @Binds
        fun bindsLocalDataSource(
            localDataSourceImpl: LocalDataSourceImp
        ): LocalDataSource

        @Binds
        fun bindRecordLocalMapper(
            CGistLocalMapper: GistLocalMapper
        ): Mapper<Gist, GistLocal>

    }

    @Provides
    @Singleton
    fun providesDatabase(application: Application) =
        GistsDB.getInstance(application.applicationContext)

    @Provides
    @Singleton
    fun providesRecordsDAO(gistsDB: GistsDB) = gistsDB.getRecordsDAO()
}