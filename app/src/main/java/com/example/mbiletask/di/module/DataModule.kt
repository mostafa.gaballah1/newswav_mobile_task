package com.example.mbiletask.di.module

import com.example.mbiletask.data.repository.GistsRepository
import com.example.mbiletask.data.repository.GistsRepositoryImp
import dagger.Binds
import dagger.Module

@Module
abstract class DataModule {

    @Binds
    abstract fun bindsRepository(
        repoImpl: GistsRepositoryImp
    ): GistsRepository
}