package com.example.mbiletask.di.module

import com.example.mbiletask.ui.GistDetailsActivity
import com.example.mbiletask.ui.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @ContributesAndroidInjector
    abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun bindDetailsActivity(): GistDetailsActivity
}