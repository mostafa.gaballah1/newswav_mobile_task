package com.example.mbiletask.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.mbiletask.ui.HomeViewModel
import com.example.mbiletask.di.qualifier.ViewModelKey
import com.example.mbiletask.ui.GistDetailsViewModel
import com.example.mbiletask.utils.ViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {
    @Binds
    abstract fun bindViewModelFactory(viewModelFactory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    abstract fun bindHomeViewModel(homeViewModel: HomeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(GistDetailsViewModel::class)
    abstract fun bindDetailsViewModel(gistDetailsViewModel: GistDetailsViewModel): ViewModel
}