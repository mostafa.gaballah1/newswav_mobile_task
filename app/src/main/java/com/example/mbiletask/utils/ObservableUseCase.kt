package com.example.mbiletask.utils

import io.reactivex.Observable
import io.reactivex.Scheduler

abstract class ObservableUseCase<T , I> constructor(
    private val backgroundScheduler: Scheduler,
    private val foregroundScheduler: Scheduler
) {
    protected abstract fun generateObservable(input: I): Observable<T>

    fun buildUseCase(input: I): Observable<T> {
        return generateObservable(input)
            .subscribeOn(backgroundScheduler)
            .observeOn(foregroundScheduler)
    }

}