package com.example.mbiletask.usecases

import com.example.mbiletask.data.remote.model.Gist
import com.example.mbiletask.data.repository.GistsRepository
import com.example.mbiletask.di.qualifier.Background
import com.example.mbiletask.di.qualifier.Foreground
import com.example.mbiletask.utils.ObservableUseCase
import io.reactivex.Observable
import io.reactivex.Scheduler
import javax.inject.Inject


class GistsUseCase @Inject constructor(
    private val gistsRepository: GistsRepository,
    @Background backgroundScheduler: Scheduler,
    @Foreground foregroundScheduler: Scheduler
) : ObservableUseCase<List<Gist>, String>(
    backgroundScheduler,
    foregroundScheduler
) {
    override fun generateObservable(input: String): Observable<List<Gist>> {
        return gistsRepository.getGists()
    }
}