package com.example.mbiletask.usecases

import com.example.mbiletask.data.remote.model.Gist
import com.example.mbiletask.data.repository.GistsRepository
import com.example.mbiletask.di.qualifier.Background
import com.example.mbiletask.di.qualifier.Foreground
import com.example.mbiletask.utils.ObservableUseCase
import io.reactivex.Observable
import io.reactivex.Scheduler
import retrofit2.Response
import javax.inject.Inject

class DeleteGistUseCase  @Inject constructor(
    private val gistsRepository: GistsRepository,
    @Background backgroundScheduler: Scheduler,
    @Foreground foregroundScheduler: Scheduler
) : ObservableUseCase<Response<Void>, Gist>(
    backgroundScheduler,
    foregroundScheduler
) {
    override fun generateObservable(input: Gist): Observable<Response<Void>> {
        return gistsRepository.removeGist(input)
    }
}