package com.example.mbiletask.usecases

import com.example.mbiletask.data.remote.model.Gist
import com.example.mbiletask.data.repository.GistsRepository
import com.example.mbiletask.di.qualifier.Background
import com.example.mbiletask.di.qualifier.Foreground
import com.example.mbiletask.utils.CompletableUseCase
import io.reactivex.Completable
import io.reactivex.Scheduler
import javax.inject.Inject

class RemoveStarActionUseCase @Inject constructor(
    private val gistsRepository: GistsRepository,
    @Background backgroundScheduler: Scheduler,
    @Foreground foregroundScheduler: Scheduler
) : CompletableUseCase<Gist>(
    backgroundScheduler,
    foregroundScheduler
) {
    override fun generateCompletable(gist: Gist): Completable {
        return gistsRepository.removeGistStarAction(gist)
    }

}