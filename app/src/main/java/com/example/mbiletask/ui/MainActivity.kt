package com.example.mbiletask.ui

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mbiletask.R
import com.example.mbiletask.data.Status
import com.example.mbiletask.data.local.GistsDAO
import com.example.mbiletask.data.remote.ApiService
import com.example.mbiletask.data.remote.model.Gist
import com.example.mbiletask.ui.adapter.GistsAdapter
import com.example.mbiletask.utils.NetworkConnection
import com.example.mbiletask.utils.ViewModelFactory
import dagger.android.support.DaggerAppCompatActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity(), GistsAdapter.GistClickListener {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    @Inject
    lateinit var apiService: ApiService


    private val homeViewModel: HomeViewModel by viewModels { viewModelFactory }
    private lateinit var gistsAdapter: GistsAdapter
    private var isDataLoaded = false
    private lateinit var gists: MutableList<Gist>

    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        observeOnRefresh()
        initRecyclerViews()
        getGistsData()
        observeOnGistsData()

        replyOfflineActions()
    }

    private fun replyOfflineActions() {
        if(NetworkConnection.verifyAvailableNetwork(this)) {
            showLoader()
            homeViewModel.replyOfflineActions()
        }
    }

    private fun observeOnRefresh() {
        refresh_view.setOnRefreshListener {
            getGistsData()
        }
    }

    private fun initRecyclerViews() {
        gistsAdapter = GistsAdapter(this)
        gists_rv.layoutManager = LinearLayoutManager(this)
        gists_rv.adapter = gistsAdapter
    }

    private fun getGistsData() {
        isDataLoaded = false
        showLoader()
        homeViewModel.getGistsData()
    }

    private fun observeOnGistsData() {
        homeViewModel.gistsLiveData.observe(this, Observer { response ->
            when (response.status) {
                Status.LOADING -> {
                    showLoader()
                }
                Status.ERROR -> {
                    hideLoader()
                }
                Status.SUCCESS -> {
                    hideLoader()
                    if (response.data?.gists.isNullOrEmpty() && !isDataLoaded) {
                    //    showNoInternetViews()
                    } else if (!isDataLoaded) {
                        setGistsData(response.data)
                    }
                }
            }
        })

        homeViewModel.changeGistStarLiveData.observe(this, Observer { response ->
            when (response.status) {
                Status.LOADING -> {
                    showLoader()
                }
                Status.SUCCESS -> {
                    hideLoader()
                    changeGistStarStatus(response.data!!.id)
                }
            }
        })

        homeViewModel.deleteGistLiveData.observe(this, Observer {response ->
            when (response.status) {
                Status.LOADING -> {
                    showLoader()
                }
                Status.SUCCESS -> {
                    hideLoader()
                    deleteGist(response.data!!.id)
                }
            }
        })

        homeViewModel.loadingLiveData.observe(this, Observer {
            hideLoader()
        })
    }

    private fun deleteGist(gistId: String) {
        val deletedGist = gists.find { it.id == gistId }
        deletedGist?.let {
            gists.remove(deletedGist)
            gistsAdapter.notifyDataSetChanged()
        }
    }

    private fun changeGistStarStatus(gistId: String) {
        val updatedGist = gists.find { it.id == gistId }
        updatedGist?.let {
            updatedGist.isStar = !updatedGist.isStar
            gistsAdapter.notifyDataSetChanged()
        }
    }


    private fun setGistsData(data: GistsPresentation?) {
        gists = data?.gists!! as MutableList<Gist>
        isDataLoaded = true
        gistsAdapter.setGistsList(gists)
    }


    private fun hideLoader() {
        progress_bar.visibility = View.GONE
        no_date_view.visibility = View.GONE
        refresh_view.isRefreshing = false
    }

    private fun showLoader() {
        progress_bar.visibility = View.VISIBLE
        no_internet_container.visibility = View.GONE
    }

    override fun onChangeStarStatus(gist: Gist) {
        showLoader()
        if(gist.isStar) {
            homeViewModel.removeGistStar(gist)
        } else {
            homeViewModel.addGistStar(gist)
        }
    }

    override fun onDeleteGist(gist: Gist) {
        homeViewModel.deleteGist(gist)
    }

    override fun onClickGist(gist: Gist) {
        val intent = Intent(this, GistDetailsActivity::class.java)
        intent.putExtra("gist", gist)
        startActivity(intent)
    }

}
