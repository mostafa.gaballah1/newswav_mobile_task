package com.example.mbiletask.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.mbiletask.R
import com.example.mbiletask.data.remote.model.Gist
import com.example.mbiletask.databinding.GistItemBinding

class GistsAdapter(var listener: GistClickListener) : RecyclerView.Adapter<GistsAdapter.ViewHolder>() {

    var items: MutableList<Gist> = mutableListOf()

    fun setGistsList(gists: MutableList<Gist>) {
        this.items = gists
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater: LayoutInflater =
            parent.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        return ViewHolder(
            GistItemBinding.inflate(layoutInflater, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding(items[position])
    }

    inner class ViewHolder(private val itemBinding: GistItemBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        fun binding(gist: Gist) {
            itemBinding.item = gist
            if(gist.isStar) {
                itemBinding.ivStar.setImageResource(R.drawable.ic_star_fill)
            } else {
                itemBinding.ivStar.setImageResource(R.drawable.ic_star)
            }

            itemBinding.ivDelete.setOnClickListener {
                listener.onDeleteGist(gist)
            }

            itemBinding.ivStar.setOnClickListener {
                listener.onChangeStarStatus(gist)
            }
            itemBinding.container.setOnClickListener {
                listener.onClickGist(gist)
            }
        }
    }


    interface GistClickListener {
        fun onChangeStarStatus(gist: Gist)
        fun onDeleteGist(gist: Gist)
        fun onClickGist(gist: Gist)
    }

}