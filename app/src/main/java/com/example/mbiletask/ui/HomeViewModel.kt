package com.example.mbiletask.ui

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.mbiletask.data.Resource
import com.example.mbiletask.data.local.GistsDAO
import com.example.mbiletask.data.remote.model.Gist
import com.example.mbiletask.usecases.*
import com.example.mbiletask.utils.Constants
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class HomeViewModel @Inject constructor(
    private val gistsUseCase: GistsUseCase,
    private val addStarUseCase: AddStarUseCase,
    private val addStarActionUseCase: AddStarActionUseCase,
    private val removeStareUseCase: RemoveStareUseCase,
    private val removeStarActionUseCase: RemoveStarActionUseCase,
    private val deleteGistUseCase: DeleteGistUseCase,
    private val deleteGistActionUseCase: DeleteGistActionUseCase,
    private val gistsDAO: GistsDAO
    ) : BaseViewModel() {

    var gistsLiveData: MutableLiveData<Resource<GistsPresentation>> = MutableLiveData()
    var changeGistStarLiveData: MutableLiveData<Resource<Gist>> = MutableLiveData()
    var deleteGistLiveData: MutableLiveData<Resource<Gist>> = MutableLiveData()
    var loadingLiveData: MutableLiveData<Boolean> = MutableLiveData()

    @SuppressLint("CheckResult")
    fun getGistsData() {

        val gists = gistsUseCase.buildUseCase("")
        gists.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                gistsLiveData.value = Resource.loading()
            }
            .subscribe(
                {
                    val gistsLiveDataNew = GistsPresentation(it)
                    gistsLiveData.value = Resource.success(gistsLiveDataNew)
                }, { error ->
                    gistsLiveData.value = Resource.error(error.message!!)
                }
            )

    }

    @SuppressLint("CheckResult")
    fun addGistStar(gist: Gist) {
        addStarUseCase.buildUseCase(gist).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                gistsLiveData.value = Resource.loading()
            }
            .subscribe(
                {
                    Log.d("zz7", "addGistStar: " + it.code())
                    changeGistStarLiveData.value = Resource.success(gist)
                    if(it.code() != 204) {
                        addStarActionUseCase.buildUseCase(gist)
                    }
                }, {
                    changeGistStarLiveData.value = Resource.success(gist)
                    addStarActionUseCase.buildUseCase(gist)
                }
            )
    }

    @SuppressLint("CheckResult")
    fun removeGistStar(gist: Gist) {
        removeStareUseCase.buildUseCase(gist).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                gistsLiveData.value = Resource.loading()
            }
            .subscribe(
                {
                    Log.d("zz7", "removeGistStar: " + it.code())
                    changeGistStarLiveData.value = Resource.success(gist)
                    if(it.code() != 204) {
                        removeStarActionUseCase.buildUseCase(gist)
                    }
                }, {
                    changeGistStarLiveData.value = Resource.success(gist)
                    removeStarActionUseCase.buildUseCase(gist)
                }
            )
    }

    @SuppressLint("CheckResult")
    fun deleteGist(gist: Gist) {
        deleteGistUseCase.buildUseCase(gist).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                gistsLiveData.value = Resource.loading()
            }
            .subscribe(
                {
                    deleteGistLiveData.value = Resource.success(gist)
                    if(it.code() != 204) {
                        deleteGistActionUseCase.buildUseCase(gist)
                    }
                }, {
                    deleteGistLiveData.value = Resource.success(gist)
                    deleteGistActionUseCase.buildUseCase(gist)
                }
            )
    }

    @SuppressLint("CheckResult")
    fun replyOfflineActions() {
        gistsDAO.getOfflineActions()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                gistsLiveData.value = Resource.loading()
            }
            .subscribe(
                {
                   it.forEach {action->
                       val gist = Gist(action.gistID, action.desc, action.isStar)
                       if(action.actionType == Constants.ADD_STAR_ACTION)
                           addStarUseCase.buildUseCase(gist)
                       else if(action.actionType == Constants.REMOVE_STAR_ACTION)
                           removeStareUseCase.buildUseCase(gist)
                       else if(action.actionType == Constants.REMOVE_ACTION)
                           deleteGistUseCase.buildUseCase(gist)
                   }
                   // loadingLiveData.value = false
                }, {
                    Log.d("www", "replyOfflineActions: ")
                   // loadingLiveData.value = false
                }
            )
    }
}