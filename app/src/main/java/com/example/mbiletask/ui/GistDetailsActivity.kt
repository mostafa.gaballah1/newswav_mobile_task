package com.example.mbiletask.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestListener
import com.example.mbiletask.R
import com.example.mbiletask.data.remote.model.Gist
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_gist_details.*

class GistDetailsActivity :  DaggerAppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gist_details)

        getExtras()
    }

    private fun getExtras() {
        val gist = intent.extras!!.getParcelable<Gist>("gist")!!
        Glide.with(this)
            .load(gist.owner.avatar_url).diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(user_iv)
        name_tv.text = gist.owner.login
        desc_tv.text = gist.description
    }
}