package com.example.mbiletask.ui

import androidx.lifecycle.MutableLiveData
import com.example.mbiletask.data.Resource
import com.example.mbiletask.data.remote.ApiService
import javax.inject.Inject

class GistDetailsViewModel @Inject constructor(
    private val apiService: ApiService
) : BaseViewModel() {

    var gistsLiveData: MutableLiveData<Resource<GistsPresentation>> = MutableLiveData()

}