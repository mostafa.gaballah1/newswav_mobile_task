package com.example.mbiletask.ui

import com.example.mbiletask.data.remote.model.Gist

data class GistsPresentation(
    val gists: List<Gist>
)