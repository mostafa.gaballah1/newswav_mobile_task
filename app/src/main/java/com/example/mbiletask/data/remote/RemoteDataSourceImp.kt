package com.example.mbiletask.data.remote

import com.example.mbiletask.data.remote.model.Gist
import io.reactivex.Observable
import retrofit2.Response
import javax.inject.Inject

class RemoteDataSourceImp @Inject constructor(
    private val apiService: ApiService
) : RemoteDataSource {

    override fun getGists(): Observable<List<Gist>> {
        return apiService.getGists()
    }

    override fun getStarredGists(): Observable<List<Gist>> {
        return apiService.getStarredGists()
    }

    override fun addGistStar(gistId: String): Observable<Response<Void>> {
        return apiService.starGist(gistId)
    }

    override fun removeGistStar(input: String): Observable<Response<Void>> {
        return apiService.unStarGist(input)
    }

    override fun removeGist(id: String): Observable<Response<Void>> {
        return apiService.deleteGist(id)
    }


}