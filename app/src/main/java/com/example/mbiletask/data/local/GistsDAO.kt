package com.example.mbiletask.data.local

import androidx.room.*
import com.example.mbiletask.data.local.model.*
import com.example.mbiletask.data.remote.model.Gist
import io.reactivex.Completable
import io.reactivex.Observable

@Dao
interface GistsDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveGists(gists: List<GistLocal>)

    @Query("select * from gistlocal")
    fun getGists(): Observable<List<GistLocal>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addAction(action: OfflineAction)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveGist(updatedGist: GistLocal)

    @Query("DELETE FROM GistLocal WHERE id = :gistId")
    fun removeGist(gistId: String): Completable

    @Query("select * from offlineaction")
    fun getOfflineActions(): Observable<List<OfflineAction>>
}