package com.example.mbiletask.data.repository

import com.example.mbiletask.data.remote.model.Gist
import io.reactivex.Completable
import io.reactivex.Observable
import retrofit2.Response

interface GistsRepository {

    fun getGists(): Observable<List<Gist>>
    fun addGistStar(gist: Gist): Observable<Response<Void>>
    fun addGistStarAction(input: Gist): Completable
    fun removeGistStar(gist: Gist): Observable<Response<Void>>
    fun removeGistStarAction(gist: Gist): Completable
    fun removeGist(input: Gist): Observable<Response<Void>>
    fun removeGistAction(input: Gist): Completable
}