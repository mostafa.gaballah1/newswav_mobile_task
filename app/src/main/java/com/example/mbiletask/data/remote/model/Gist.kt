package com.example.mbiletask.data.remote.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Gist(
    var id: String,
    var description: String,
    var isStar: Boolean,
    var owner: GistOwner = GistOwner("owner", "https://avatars1.githubusercontent.com/u/20671485?v=4")
) : Parcelable