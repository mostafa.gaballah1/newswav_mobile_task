package com.example.mbiletask.data.local.mapper

import com.example.mbiletask.data.local.model.GistLocal
import com.example.mbiletask.data.remote.model.Gist
import com.example.mbiletask.utils.Mapper
import javax.inject.Inject

class GistLocalMapper @Inject constructor() : Mapper<Gist, GistLocal> {

    override fun from(e: GistLocal): Gist {
        return Gist(
            id = e.id,
            description = e.description,
            isStar = e.isStar
        )
    }

    override fun to(t: Gist): GistLocal {
        return GistLocal(
            id = t.id,
            description = t.description,
            isStar = t.isStar
        )
    }
}