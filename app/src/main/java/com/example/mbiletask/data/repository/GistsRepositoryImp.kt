package com.example.mbiletask.data.repository

import android.annotation.SuppressLint
import android.util.Log
import com.example.mbiletask.data.local.LocalDataSource
import com.example.mbiletask.data.local.model.OfflineAction
import com.example.mbiletask.data.remote.RemoteDataSource
import com.example.mbiletask.data.remote.model.Gist
import com.example.mbiletask.utils.Constants
import io.reactivex.Completable
import io.reactivex.CompletableObserver
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.functions.BiFunction
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import retrofit2.Response
import javax.inject.Inject


class GistsRepositoryImp @Inject constructor(
    private val remoteDataSource: RemoteDataSource,
    private val localDataSource: LocalDataSource
) : GistsRepository {

    @SuppressLint("CheckResult")
    override fun getGists(): Observable<List<Gist>> {

        val allGists = remoteDataSource.getGists()
            .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
        val starredGists = remoteDataSource.getStarredGists()
            .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())


        val localSource = localDataSource.getGists()
        return Observable.zip(allGists, starredGists,
            BiFunction<List<Gist>, List<Gist>, List<Gist>> { t1, t2 -> setStarredGists(t1, t2)}).map {

            Completable.fromAction {
                localDataSource.saveGists(it)
            }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(object : CompletableObserver {
                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}
                    override fun onError(e: Throwable) {}
                })


            it
        }.onErrorResumeNext(Observable.empty()).concatWith(localSource)
    }

    @SuppressLint("CheckResult")
    override fun addGistStar(gist: Gist): Observable<Response<Void>> {
        val updatedGist = Gist(gist.id, gist.description, true)
        Completable.fromAction {
            localDataSource.saveGist(updatedGist)
        }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io()).subscribeOn(Schedulers.io()).subscribe(object : CompletableObserver {
                override fun onSubscribe(d: Disposable) {}
                override fun onComplete() {}
                override fun onError(e: Throwable) {}
            })
        return remoteDataSource.addGistStar(gist.id)
    }

    @SuppressLint("CheckResult")
    override fun removeGistStar(gist: Gist): Observable<Response<Void>> {
        val updatedGist = Gist(gist.id, gist.description, false)
        Completable.fromAction {
            localDataSource.saveGist(updatedGist)
        }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io()).subscribeOn(Schedulers.io()).subscribe(object : CompletableObserver {
                override fun onSubscribe(d: Disposable) {}
                override fun onComplete() {}
                override fun onError(e: Throwable) {}
            })
        return remoteDataSource.removeGistStar(gist.id)
    }

    override fun removeGistStarAction(input: Gist): Completable {
        val action = OfflineAction(input.id, input.description, input.isStar, Constants.REMOVE_STAR_ACTION)
        return Completable.fromAction {
            localDataSource.addAction(action)
        }
    }

    override fun removeGist(input: Gist): Observable<Response<Void>> {
        Completable.fromAction {
            localDataSource.removeGist(input)
        }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io()).subscribeOn(Schedulers.io()).subscribe(object : CompletableObserver {
                override fun onSubscribe(d: Disposable) {}
                override fun onComplete() {}
                override fun onError(e: Throwable) {}
            })
        return remoteDataSource.removeGist(input.id)
    }

    override fun removeGistAction(input: Gist): Completable {
        val action = OfflineAction(input.id, input.description, input.isStar, Constants.REMOVE_ACTION)
        return Completable.fromAction {
            localDataSource.addAction(action)
        }
    }

    override fun addGistStarAction(input: Gist): Completable {
        val action = OfflineAction(input.id , input.description, input.isStar, Constants.ADD_STAR_ACTION)
        return Completable.fromAction {
            localDataSource.addAction(action)
        }
    }

    private fun setStarredGists(allGists: List<Gist>, starredGists: List<Gist>): List<Gist>{
        allGists.forEach { gist ->
            val starredGist = starredGists.find { it.id == gist.id }
            if(starredGist != null) {
                gist.isStar = true
            }
        }
        return allGists
    }

}