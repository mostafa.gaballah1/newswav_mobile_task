package com.example.mbiletask.data.remote

import com.example.mbiletask.data.remote.model.Gist
import io.reactivex.Observable
import retrofit2.Response

interface RemoteDataSource {

    fun getGists(): Observable<List<Gist>>

    fun getStarredGists(): Observable<List<Gist>>
    fun addGistStar(gistId: String): Observable<Response<Void>>
    fun removeGistStar(input: String): Observable<Response<Void>>
    fun removeGist(id: String): Observable<Response<Void>>
}