package com.example.mbiletask.data.local.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.mbiletask.data.remote.model.GistOwner

@Entity
data class GistLocal(
    @PrimaryKey val id: String,
    var description: String,
    var isStar: Boolean
)