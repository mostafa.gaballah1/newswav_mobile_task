package com.example.mbiletask.data.local

import android.content.Context
import androidx.annotation.NonNull
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.mbiletask.data.local.model.*

@Database(
    entities = [GistLocal::class, OfflineAction::class],
    version = 1)
abstract class GistsDB : RoomDatabase() {

    companion object {
        private val LOCK = Any()
        private const val DATABASE_NAME = "gists.db"

        @Volatile
        private var INSTANCE: GistsDB? = null

        fun getInstance(@NonNull context: Context): GistsDB {
            if (INSTANCE == null) {
                synchronized(LOCK) {
                    if (INSTANCE == null) {
                        INSTANCE = Room.databaseBuilder(
                            context,
                            GistsDB::class.java,
                            DATABASE_NAME
                        ).fallbackToDestructiveMigration().build()
                    }
                }
            }
            return INSTANCE!!
        }
    }

    abstract fun getRecordsDAO(): GistsDAO
}