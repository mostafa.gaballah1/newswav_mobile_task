package com.example.mbiletask.data.remote.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GistOwner(
    var login: String,
    var avatar_url: String
) : Parcelable