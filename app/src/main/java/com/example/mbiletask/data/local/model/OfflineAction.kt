package com.example.mbiletask.data.local.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class OfflineAction(
    @PrimaryKey val gistID: String,
    val desc: String,
    val isStar: Boolean,
    val actionType: Int
)