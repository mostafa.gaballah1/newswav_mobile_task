package com.example.mbiletask.data.remote

import com.example.mbiletask.data.remote.model.Gist
import com.example.mbiletask.utils.Constants.TOKEN
import com.example.mbiletask.utils.Constants.USER
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.PUT
import retrofit2.http.Path

interface ApiService {

    @GET("users/$USER/gists?access_token=$TOKEN")
    fun getGists(): Observable<List<Gist>>

    @PUT("gists/{gist_id}/star?access_token=$TOKEN")
    fun starGist(@Path("gist_id") gistId: String): Observable<Response<Void>>

    @DELETE("gists/{gist_id}/star?access_token=$TOKEN")
    fun unStarGist(@Path("gist_id") gistId: String): Observable<Response<Void>>

    @GET("gists/starred?access_token=$TOKEN")
    fun getStarredGists(): Observable<List<Gist>>

    @DELETE("gists/{gist_id}?access_token=$TOKEN")
    fun deleteGist(@Path("gist_id") gist_id: String): Observable<Response<Void>>

}