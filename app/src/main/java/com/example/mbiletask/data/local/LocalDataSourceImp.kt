package com.example.mbiletask.data.local

import com.example.mbiletask.data.local.mapper.GistLocalMapper
import com.example.mbiletask.data.local.model.GistLocal
import com.example.mbiletask.data.local.model.OfflineAction
import com.example.mbiletask.data.remote.model.Gist
import io.reactivex.Completable
import io.reactivex.Observable
import javax.inject.Inject

class LocalDataSourceImp @Inject constructor(
    private val gistsDAO: GistsDAO,
    private val gistLocalMapper: GistLocalMapper
) : LocalDataSource {

    override fun saveGists(gists: List<Gist>) {
        gistsDAO.saveGists(
            gists.map {
                gistLocalMapper.to(it)
            }
        )
    }

    override fun getGists(): Observable<List<Gist>> {
        return gistsDAO.getGists()
            .map { records ->
                records.map {
                    gistLocalMapper.from(it)
                }
            }
    }

    override fun addAction(action: OfflineAction) {
        gistsDAO.addAction(action)
    }

    override fun saveGist(updatedGist: Gist) {
        gistsDAO.saveGist(gistLocalMapper.to(updatedGist))
    }

    override fun removeGist(input: Gist): Completable{
        return gistsDAO.removeGist(input.id)
    }


}