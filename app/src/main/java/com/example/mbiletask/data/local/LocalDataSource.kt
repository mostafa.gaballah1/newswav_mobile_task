package com.example.mbiletask.data.local

import com.example.mbiletask.data.local.model.GistLocal
import com.example.mbiletask.data.local.model.OfflineAction
import com.example.mbiletask.data.remote.model.Gist
import io.reactivex.Completable
import io.reactivex.Observable

interface LocalDataSource {

    fun saveGists(gists: List<Gist>)

    fun getGists(): Observable<List<Gist>>
    fun addAction(action: OfflineAction)
    fun saveGist(updatedGist: Gist)
    fun removeGist(input: Gist): Completable

}