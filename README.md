You just got hired by GitHub, and for your first task, you need to build a small app that will read a list of user Gists, display them, and let the user make the following actions on his gists:
- Star a gist.
- Unstar a gist.
- Delete a gist.
The requirements for this app are as the following:
- User can click on a gist from the list to view it's content.
- The gists list should be cached on the disk and refreshed on app startup.
- User can use the app offline, meaning that when there is no internet connection user can do actions on any gist, and the app should cache those actions and replay them when the internet connection is back on.
- When replaying actions, actions on different gists should be sent to the server in parallel since they are independent from each other. While actions made on the same gist should be sent serially to the server, based on the order they were made.
For example, imagine the user had two gists, GistA and GistB, and while the device is offline, user made the following actions:
On GistA user made ActionA1, ActionA2, and ActionA3 and on GistB user made ActionB1 and ActionB2.
When the device becomes online and you want to send the actions to the server, ActionA1, ActionA2 and ActionA3 should be sent to the server one after the other, and at the same time ActionB1 and ActionB2 should be sent to server one after the other.
- When the user does an action while the device is offline, the UI should be updated normally as if the device was online.
- Replaying actions is blocking, so while sending the actions that were made offline, it is ok to block the user from interacting with the app by showing a loading indicator.
- After replaying all offline actions, the list of gists should be refreshed so the user sees the most up to date version of his gists.
- The app does not have to handle network failures or server errors while replaying actions, so once an action was sent you can consider it synced.
- You can work under the assumption that only you are  modifying the gists (you don't have to fetch gists periodically).
Notes about the test:
- You are free to use any UI elements you find suitable.
- You are free to use any third party libraries.
- Build the app for portrait mode only.
- Is better to have clean architecture for this app, like using MVVM or MVP and Rx.
- You can assume that access token can be given on code level, no need to hardcode any access token, when our engineers test they will put their access token in hardcoded way, no need to build any custom UI to authenticate with github.


Notes about Github API:
- First, go to https://gist.github.com/ and create some gists!
- There is no need to do any login/logout mechanism in the app, to use the Github API you only need an access token, please go to https://github.com/settings/tokens and generate one for your account.
- The API's you will need are,
https://developer.github.com/v3/gists/#list-gists
https://developer.github.com/v3/gists/#get-a-single-gist
https://developer.github.com/v3/gists/#star-a-gist
https://developer.github.com/v3/gists/#unstar-a-gist
https://developer.github.com/v3/gists/#delete-a-gist
- To get a list of gists for example, you can call https://api.github.com/users/[your_username]/gists?access_token=[your_access_token]
- To get a specific gist for example, you can call https://api.github.com/gists/[gist_id]?access_token=[your_access_token]
- To show a gist in the app, it is enough to display the response returned from https://developer.github.com/v3/gists/#get-a-single-gist as it is.

You can host the project on github and send us the repo after that, please make sure the project is compatible with Android Studio 3.5+  version, is okay to use external frameworks and libraries.

Please understand that this test becomes the showcase of your skills, so don't skip on professional programming standards (intentional vague).
If you have any questions about the requirements please ask us before proceeding with the test implementation.

